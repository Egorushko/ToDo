import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { CheckBox, ListItem, View,
  Header, Left, Right, FooterTab,
  Body, Title, Item, Footer,
  Button, Text, Icon, Input, Segment,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
// import { green } from 'color';

const deviceWidth = Dimensions.get('window').width;

export default class AddTask extends Component {

  constructor() {
    super();

    this.state = {
      bookmarkIsActive: false,
      date: '',
      bellIsActive: false,
      type: 'Personal',
    };
  }

  render() {
    const {
      text1, isChecked, invertIsChecked,
      date, type, bellIsActive, bookmarkIsActive,
    } = this.props.comp;
    // '\nkejlekr'
    const textTask = text1.concat('\n');
    const subText = date.concat('       ').concat(type);
    const { myKey } = this.props;
    const styleLine = isChecked ? { textDecorationLine: 'line-through' } : { textDecorationLine: 'none' };
    return (
      <ListItem
      icon
      onPress={() => { this.onPressItem(myKey); }}
      >
        <Left>
          <CheckBox checked={isChecked} onPress={() => {
            invertIsChecked();
            this.forceUpdate();
          } }/>
        </Left>
        <Body>
          <Text note >
            <Text style={styleLine} >
              {textTask}
            </Text>
            {subText}
          </Text>
        </Body>
        <Right>
          <Button transparent
          >
            <Icon name={bookmarkIsActive ? 'ios-bookmark' : 'ios-bookmark-outline' } />
          </Button>
          <Button transparent
          >
            <Icon name={bellIsActive ? 'ios-notifications' : 'ios-notifications-outline'} />
          </Button>
        </Right>
      </ListItem>
    );
  }

  onPressItem(key) {
    this.props.navigation.navigate('ScreenView2', { index: key });
  }
}
