import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { Container, Content, View,
  Header, Left, Right, FooterTab,
  Body, Title, Item, Footer,
  Button, Text, Icon, Input, Segment,
} from 'native-base';
import DatePicker from 'react-native-datepicker';
import { observer, inject } from 'mobx-react/native';
// import { green } from 'color';

const deviceWidth = Dimensions.get('window').width;

@inject(['toDoStore'])
@observer
export default class AddTask extends Component {
  constructor(props) {
    super(props);
    let index;
    // try {
    //   if (navigate !== undefined) {
    //     index = this.props.navigate.state.params.index;
    //   }
    // }

    if (this.props.navigation.state.params !== undefined) {
      index = this.props.navigation.state.params.index;
    }

    this.addNewTask = this.addNewTask.bind(this);
    this.changeTask = this.changeTask.bind(this);

    if (index === undefined) {
      this.state = {
        id: index,
        bookmarkIsActive: false,
        date: '',
        bellIsActive: false,
        type: 'Personal',
        text1: '',
        headerText: 'Add New Task',
      };
    } else {
      const { todos } = this.props.toDoStore;
      this.state = {
        id: index,
        bookmarkIsActive: todos[index].bookmarkIsActive,
        date: todos[index].date,
        bellIsActive: todos[index].bellIsActive,
        type: todos[index].type,
        headerText: 'Change Task',
        text1: todos[index].text1,
      };
    }
  }

  render() {
    const {
      bellIsActive, bookmarkIsActive, type, headerText,
      text1, id,
    } = this.state;
    return (
            <Container>
              <Header>
              <Left>
                <Button transparent
                  onPress={() => this.props.navigation.goBack()}
                  >
                    <Icon name='md-arrow-back' />
                  </Button>
                </Left>
                <Body>
                  <Title>{headerText}</Title>
                </Body>
                <Right>
                {/* <Button
                  onPress={() => this.props.navigation.navigate('Bar')}
                  >
                    <Text>
                      Следующий
                    </Text>
                  </Button> */}
                </Right>
              </Header>
              <Content>
                <Item>
                  <Input multiline placeholder='Input Task'
                  value={(text1 !== '') ? text1 : null }
                  onChangeText={tex => this.setState({ text1: tex }) }
                  // editable={true}
                  />
                </Item>
                <View lineDown>
                <DatePicker
                  style={{
                    width: deviceWidth,
                    paddingHorizontal: 50,
                  }}
                  date={this.state.date}
                  mode="datetime"
                  // maxDate="01.01.2010"
                  // iconSource={<Icon name={'ios-notifications'} />}
                  placeholder="Date"
                  // format="MMMM Do YYYY, h:mm:ss a"
                  format="DD.MM.YYYY HH:mm"
                  // minDate="2016-05-01"
                  // maxDate="2016-06-01"
                  confirmBtnText="Ok"
                  cancelBtnText="Cansel"
                  customStyles={{
                  //   dateIcon: {

                  //     // right: 20,
                  //     top: 0,
                  //     marginLeft: 0,
                  //     width: 8,
                  //     height: 13,


                  //   },
                  //   dateTouchBody: {

                  //   },
                    dateInput: {
                      // marginLeft: 36,
                      borderWidth: 0,
                      alignItems: 'flex-start',
                    },

                  }}
                  onDateChange={(dateVal) => { this.setState({ date: dateVal }); } }
                />
                </View>
                <Segment>
                  <Button transparent first
                  active={(type === 'Personal')}
                  onPress={() => this.setState({ type: 'Personal' }) }
                  >
                    <Text>
                      Personal
                    </Text>
                  </Button >
                  <Button transparent
                  active={(type === 'Work')}
                  onPress={() => this.setState({ type: 'Work' }) }
                  >
                    <Text>
                      Work
                    </Text>
                  </Button>
                  <Button transparent last
                  active={(this.state.type === 'Meet')}
                  onPress={() => this.setState({ type: 'Meet' }) }
                  >
                    <Text>
                      Meet
                    </Text>
                  </Button>
                </Segment>
                <View rowBetween>
                  <Button transparent
                  onPress={() => this.setState({ bookmarkIsActive: !this.state.bookmarkIsActive }) }
                  >
                    <Icon name={bookmarkIsActive ? 'ios-bookmark' : 'ios-bookmark-outline' } />
                  </Button>
                  <Button transparent
                  onPress={() => this.setState({ bellIsActive: !this.state.bellIsActive }) }
                  >
                    <Icon name={bellIsActive ? 'ios-notifications' : 'ios-notifications-outline'} />
                  </Button>
                </View>
              </Content>
              <Footer>
                <FooterTab>
                  <Button full
                  // style={{ backgroundColor: green }}
                  onPress={(id === undefined) ? this.addNewTask : this.changeTask}
                  >
                    <Text>{(id === undefined) ? 'Add Task' : 'Change Task' }</Text>
                  </Button>
                </FooterTab>
              </Footer>
            </Container>
    );
  }
  // eslint-disable-next-line class-methods-use-this
  addNewTask() {
    const { AddTask, SaveData, beforeDestroy } = this.props.toDoStore;
    const {
      state,
    } = this;
    const newTask = {
      text1: state.text1,
      isChecked: false,
      bookmarkIsActive: state.bookmarkIsActive,
      bellIsActive: state.bellIsActive,
      date: state.date,
      type: state.type,
    };
    AddTask(newTask);
    // SaveData();
    // beforeDestroy();
    this.props.navigation.goBack();
  }
  changeTask() {
    const { ChangeTask, beforeDestroy } = this.props.toDoStore;
    const {
      state,
    } = this;
    const newTask = {
      text1: state.text1,
      isChecked: false,
      bookmarkIsActive: state.bookmarkIsActive,
      bellIsActive: state.bellIsActive,
      date: state.date,
      type: state.type,
    };
    ChangeTask(state.id, newTask);
    // beforeDestroy();
    this.props.navigation.goBack();
  }
}
