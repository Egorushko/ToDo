import React, { Component } from 'react';
import { Container, Content,
  Header, Left, Right, Icon,
  Body, Title, Button, Text,
  Card, List, ListItem, CheckBox, SubTitle,
  Footer, FooterTab,
} from 'native-base';
import { observer, inject } from 'mobx-react/native';

import TaskRow from './TaskRow';

let lastDate = '';

@inject(['toDoStore'])
@observer
export default class View1 extends Component {
  render() {
    const { todos, LoadData } = this.props.toDoStore;
    return (
            <Container>
              <Header>
                <Left>
                <Button transparent
                  onPress={LoadData}
                  >
                    <Icon name='ios-checkmark-circle-outline' />
                  </Button>
                </Left>
                <Body>
                  <Title>My Tasks</Title>
                </Body>
                <Right>
                  <Button transparent
                  // onPress={() => this.props.navigation.navigate('ScreenView2')}
                  >
                    <Icon name='settings' />
                  </Button>
                </Right>
              </Header>
              <Content
              // style={{ backgroundColor: '#faa' }}
              >
                <Card>
                  {this.sortRow()}
                </Card>
              </Content>
              <Footer>
                <FooterTab>
                  <Button full
                  onPress={() => this.props.navigation.navigate('ScreenView2')}
                  >
                    <Icon name='md-add-circle' />
                  </Button>
                </FooterTab>
              </Footer>
            </Container>
    );
  }

  sortRow() {
    const { todos } = this.props.toDoStore;
    const array = todos.map(compToSort => compToSort);
    // array = array.sort((comp1, comp2) => comp1.date.localeCompare(comp2.date));
    const a = array.map((compSort, indexSort) => this.categoriesRow(compSort, indexSort));
    return (a);
  }

  // eslint-disable-next-line class-methods-use-this
  categoriesRow(comp, index) {
    // const { text1, isChecked, invertIsChecked } = comp;
    const rend = [];
    const modDate = comp.date.split(' ', 1);
    if (modDate[0] === lastDate) {
      rend.push(<TaskRow comp={comp} key={index}
        myKey={index} navigation={this.props.navigation} />);
    } else {
      lastDate = modDate[0];
      const divider = <ListItem key={index + 1000} itemDivider>
             <Text>{lastDate}</Text>
           </ListItem>;
      rend.push(divider);
      rend.push(<TaskRow comp={comp} key={index}
        myKey={index} navigation={this.props.navigation} />);
    }
    return (rend);
  }
}
