import { types, getSnapshot, applySnapshot, onAction } from 'mobx-state-tree';
import makeInspectable from 'mobx-devtools-mst';
import { AsyncStorage } from 'react-native';
// import { autorun } from 'mobx';

const storeKey = '@Stores:todoArray';
let isFirstRun = true;

const ToDo = types
  .model('ToDo', {
    text1: types.string,
    isChecked: types.boolean,
    bookmarkIsActive: types.boolean,
    bellIsActive: types.boolean,
    date: types.string,
    type: types.string,
  })
  // .views(self => ({
  //   get text2() {
  //     const { text1 } = self;
  //     return text1.concat(' привет!');
  //   },
  // }))
  .actions((self) => {
    function setText1(text) {
      self.text1 = text;
    }
    function invertIsChecked() {
      const b = self.isChecked;
      self.isChecked = !b;
    }
    return { setText1, invertIsChecked };
  });

const ToDoArray = types
  .model('ToDoArray', {
    todos: types.array(ToDo),
  })
  .actions((self) => {
    async function afterCreate() {
      const jsonData = await AsyncStorage.getItem(storeKey);
      const snapshot = JSON.parse(jsonData);
      applySnapshot(self, snapshot);
    }
    function AddTask(toDo) {
      self.todos.push(toDo);
      self.todos = self.todos.sort((comp1, comp2) => comp1.date.localeCompare(comp2.date));
    }
    function ChangeTask(index, newTask) {
      self.todos[index] = {
        text1: newTask.text1,
        isChecked: self.todos[index].isChecked,
        bookmarkIsActive: newTask.bookmarkIsActive,
        bellIsActive: newTask.bellIsActive,
        date: newTask.date,
        type: newTask.type,
      };
      self.todos = self.todos.sort((comp1, comp2) => comp1.date.localeCompare(comp2.date));
    }

    return {
      AddTask,
      ChangeTask,
      afterCreate,
    };
  });


const ToDoStore = ToDoArray.create({ todos: [] });

// Save after property changed
onAction(
  ToDoStore,
  async (call) => {
    if (isFirstRun !== true) {
      const snapshot = getSnapshot(ToDoStore);
      const jsonData = JSON.stringify(snapshot);
      AsyncStorage.setItem(storeKey, jsonData);
    }
    isFirstRun = false;
  },
  true,
);

// autorun(() => {
//   console.log('There are now ', ToDoArray.amountOfChildren, ' children');
//   // ToDoArray.actions.beforeDestroy();
// });

makeInspectable(ToDoStore);
export default ToDoStore;
