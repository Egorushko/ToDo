import ToDoStore from './ToDoStore';

export default function () {
  const toDoStore = ToDoStore;

  return {
    toDoStore,
  };
}
