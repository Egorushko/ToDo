import React, { Component } from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { Footer, FooterTab, Button, Icon } from 'native-base';

import MyTasks from './View/MyTasks';
import AddTask from './View/AddTask';
import View3 from './View/View3';
import View4 from './View/View4';
import View5 from './View/View5';

function tabBar(props) {
  return <Footer>
          <FooterTab>
            <Button
                active={props.navigationState.index === 0}
                onPress={() => props.navigation.navigate('ScreenView3')}
            >
                <Icon name='ios-help-circle'/>
            </Button>
            <Button
                active={props.navigationState.index === 1}
                onPress={() => props.navigation.navigate('ScreenView4')}
            >
                <Icon name='ios-stats'/>
            </Button>
            <Button
                active={props.navigationState.index === 2}
                onPress={() => props.navigation.navigate('ScreenView5')}
            >
                <Icon name='md-list-box'/>
            </Button>
          </FooterTab>
        </Footer>;
}
const RootNavigator = StackNavigator({
  ScreenView1: {
    screen: MyTasks,
    navigationOptions: {
      header: null,
      // gesturesEnabled: true,
    },
  },
  ScreenView2: {
    screen: AddTask,
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
  Bar: {
    screen: TabNavigator(
      {
        ScreenView3: {
          screen: View3,
          navigationOptions: {
            header: null,
            gesturesEnabled: true,
          },
        },
        ScreenView4: {
          screen: View4,
          navigationOptions: {
            header: null,
            gesturesEnabled: true,
          },
        },
        ScreenView5: {
          screen: View5,
          navigationOptions: {
            header: null,
            gesturesEnabled: true,
          },
        },
      },
      {
        initialRouteName: 'ScreenView3',
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarComponent: tabBar,
      },
    ),
  },
});

export default RootNavigator;
