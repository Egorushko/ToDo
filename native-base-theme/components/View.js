import variable from './../variables/platform';

export default (variables = variable) => {
  const viewTheme = {
    '.padder': {
      padding: variables.contentPadding
    },
    '.rowBetween': {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 100,
      paddingVertical: 5,
      borderBottomWidth: 1,
      borderTopWidth: 1,
      borderColor: variables.inputBorderColor,
    },
    '.lineDown': {
      // flexDirection: 'row',
      // justifyContent: 'space-between',
      // paddingHorizontal: 100,
      // paddingVertical: 5,
      borderBottomWidth: 1,
      // borderTopWidth: 1,
      borderColor: variables.inputBorderColor,
    },
  };

  return viewTheme;
};
