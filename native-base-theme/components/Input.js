import variable from './../variables/platform';

export default (variables = variable) => {
  const inputTheme = {
    '.multiline': {
      height: null,
      minHeight: 250,
    },
    height: variables.inputHeightBase,
    color: variables.inputColor,
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
    fontSize: variables.inputFontSize,
    lineHeight: variables.inputLineHeight,
  };

  return inputTheme;
};
