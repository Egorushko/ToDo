import React, { Component } from 'react';
import { Font } from 'expo';
import { Root, StyleProvider } from 'native-base';
import { Provider } from 'mobx-react/native';

import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import Store from './src/Store';

import Main from './src/Main';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fontAndImagesIsLoaded: false,
    };
  }

  render() {
    // if Font is down load, we render the App

    if (!this.state.fontAndImagesIsLoaded) {
      return null;
    }
    const store = Store();
    return (
        <Root>
          <StyleProvider style={getTheme(platform)}>
            <Provider {...store}>
              <Main/>
            </Provider>
          </StyleProvider>
        </Root>
    );
  }

  async componentDidMount() {
    // Load 'Roboto' and 'PTRoubleSans' font
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
    });

    // When loaded, then render App
    this.setState({
      fontAndImagesIsLoaded: true,
    });
    console.log('App.js:componentDidMount finishing');
  }
}
